package vendor.dolby.ms12@1.0;

enum Result : uint32_t {
    OK,
    ERROR,
    NOT_SUPPORTED
};

enum DeviceType : uint32_t {
    SPEAKER,
    HEADPHONE
};

enum VirtualizerMode : uint32_t {
    DISABLE,
    ENABLE,
    ENABLE_FOR_ATMOS
};

enum DrcMode : uint32_t {
    LINE,
    RF
};

enum VolumeLevelerMode : uint32_t {
    DISABLE,
    ENABLE,
    ENABLE_FOR_UNLEVELED
};

enum PresentationMode : uint32_t {
    LANGUAGE,
    ASSOCIATED_TYPE
};

enum PresentationAssociatedType : uint32_t {
    NONE,
    VISUALLY_IMPAIRED,
    HEARING_IMPAIRED,
    COMMENTARY
};

enum OutputChannelMode : uint32_t {
    AUTO,
    FIVE_POINT_ONE
};

enum Feature : uint32_t {
    AAC_PROGRAM_REFERENCE_LEVEL,
    AC4_DIALOGUE_ENHANCER,
    AC4_PRESENTATION_PREFERENCE,
    ATMOS_INDICATOR,
    BITSTREAM_OUTPUT_ATMOS_LOCK,
    BITSTREAM_OUTPUT_CHANNEL_MODE,
    DIALOGUE_ENHANCER,
    DOWNMIX_MODE,
    DRC_BOOST_STRENGTH_MULTICHANNEL,
    DRC_BOOST_STRENGTH_STEREO,
    DRC_CUT_STRENGTH_MULTICHANNEL,
    DRC_CUT_STRENGTH_STEREO,
    DRC_MODE_MULTICHANNEL,
    DRC_MODE_STEREO,
    VIRTUALIZER,
    VIRTUALIZER_HEADPHONE_REVERB,
    VIRTUALIZER_SPEAKER_ANGLE,
    VIRTUALIZER_SPEAKER_START_FREQUENCY,
    VIRTUALIZER_SURROUND_BOOST,
    VOLUME_LEVELER
};

enum AudioDumpFileMode : uint32_t {
    WRITE,
    APPEND
};

enum DownmixMode : uint32_t {
    LTRT,
    LORO,
    ARIB
};
